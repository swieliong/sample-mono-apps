/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sample.apps.web.rest.vm;
